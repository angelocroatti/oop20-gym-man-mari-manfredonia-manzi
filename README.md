# Gym Man
Software gestionale per palestre.

Autori: Mattia Mari, Daniele Manfredonia, Matteo Manzi

Avvertenza: è necessario installare [Lombok](https://projectlombok.org/setup/eclipse) per far si che Eclipse funzioni correttamente.

## Demo
Sono disponibili 4 impiegati demo:

Gestore: ha tutti i permessi.  
Username: ```gestore``` Password: ```admin123```


Mario Rossi e Luca Bianchi: hanno il ruolo "cassa", possono solo gestire le iscrizioni.  
Username: ```mrossi``` Password: ```mrossi123```  
Username: ```lbianchi``` Password: ```lbianchi123```


Giovanni Neve: tecnico, può solo gestire gli attrezzi.  
Username: ```gneve``` Password: ```gneve123```


Cliente demo per l'accesso come cliente: username ```mvale``` password ```mvale123```